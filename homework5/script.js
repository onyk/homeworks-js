function createNewUser() {
    const firstName = prompt('firstName');
    const lastName = prompt('lastName');
    const birthday = prompt('birthday');
    let newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },
        getAge() {
            const toDay = new Date();
            return toDay.getFullYear()-this.birthday.split('.')[2];
        },
        getPassword(){
            return this.firstName.charAt(0).toUpperCase()+ this.lastName.toLowerCase()+this.birthday.split('.')[2];
        }
    };
    return newUser;
}
const user1 = createNewUser();
console.log(user1.getPassword());
console.log(user1.getAge());
console.log(user1.getLogin());

